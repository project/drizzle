<?php

/**
 * @file
 * Configuration page for drizzle module.
 */

/**
 * Form constructor for the admin drizzle key configuration form.
 */
function drizzle_admin_settings($form, &$form_state) {
  $form = array();
  $t = get_t();
  $api_key = drizzle_get_api_key();
  $is_key_valid = $api_key && drizzle_verify_key($api_key) == 'valid';
  // Check if API is exist and valid.
  if ($api_key) {
    if ($is_key_valid) {
      drupal_set_message($t('Connected to Drizzle'), 'status', FALSE);
    }
    else {
      drupal_set_message($t('API Key is invalid!'), 'error', FALSE);
    }
  }

  $form['steps'] = array(
    '#markup' => $t('<h3 class="hndle"><span>Settings</span></h3>
     <p style="margin-left: 20px">1. Register at <a href="@link" target="_blank">Drizzle</a>.</p>
     <p style="margin-left: 20px">2. Generate API key at "Setup" tab. Copy API key. Paste API key below. Save.</p>', array('@link' => 'https://getdrizzle.com')),
  );
  $form['drizzle_key'] = array(
    '#type' => 'textfield',
    '#title' => $t('API Key'),
    '#default_value' => variable_get('drizzle_key', ''),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
