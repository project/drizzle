<?php

/**
 * @file
 * Drizzle module.
 */

define('DRIZZLE_API_HOST', 'app.getdrizzle.com');
define('DRIZZLE_API_URL', 'https://app.getdrizzle.com/wp-api/v1');
define('DRIZZLE_API_PORT', 80);
define('DRIZZLE_NONCE', 'drizzle-update-key');
define('DRIZZLE_VERSION', '1.2.1');
define('DRIZZLE__MINIMUM_WP_VERSION', '4.0');
define('DRIZZLE_WIDGET_SCRIPT_URL', 'https://s3-us-west-1.amazonaws.com/zenmarket/for-widget.js');

/**
 * Implements hook_help().
 */
function drizzle_help($path, $arg) {
  switch ($path) {
    case 'admin/help#drizzle':
      $module_path = drupal_get_path('module', 'drizzle');
      return check_markup(file_get_contents($module_path . "/README.txt"));
  }
}

/**
 * Implements hook_page_alter().
 */
function drizzle_page_alter(&$page) {
  $key = drizzle_get_api_key();
  $script = "";
  $valid_key = drizzle_verify_key($key);
  if ($key && $valid_key == 'valid') {
    $url = DRIZZLE_WIDGET_SCRIPT_URL;
    $script .= "(function() {";
    $script .= "  var script = document.createElement('script');";
    $script .= "  script.src = '{$url}';";
    $script .= "  script.async = true;";
    $script .= "  var entry = document.getElementsByTagName('script')[0];";
    $script .= "  entry.parentNode.insertBefore(script, entry);";
    $script .= "})();";
    drupal_add_js($script, 'inline');
  }
}

/**
 * Implements hook_permission().
 */
function drizzle_permission() {
  return array(
    'administer drizzle' => array(
      'title' => t('Administer permission for Drizzle module'),
      'description' => t('Provide Drizzle content access permission to users'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function drizzle_menu() {
  $items = array();
  $items['admin/config/drizzle'] = array(
    'title' => 'Drizzle',
    'description' => 'Drizzle Configuration',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('drizzle_admin_settings'),
    'file' => 'includes/drizzle.admin.inc',
    'access arguments' => array('administer drizzle'),
    'weight' => -20,
  );
  $items['admin/config/drizzle/key-config'] = array(
    'title' => 'Drizzle Key Configuration',
    'description' => 'Administer drizzle settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('drizzle_admin_settings'),
    'file' => 'includes/drizzle.admin.inc',
    'access arguments' => array('administer drizzle'),
  );
  $items['admin/config/drizzle/content'] = array(
    'title' => 'Drizzle Content',
    'description' => 'Find and manage content.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('node_admin_content'),
    'file' => 'node.admin.inc',
    'file path' => drupal_get_path('module', 'node'),
    'access arguments' => array('administer drizzle'),
  );
  return $items;
}

/**
 * Implements hook_node_view().
 */
function drizzle_node_view($node, $view_mode, $langcode) {
  $content = '';
  if (isset($node->body)) {
    $langcode = key($node->body);
  }
  if (isset($node->body[$langcode][0]['value'])) {
    $content = $node->body[$langcode][0]['value'];
  }
  if (empty($content)) {
    return;
  }
  $shortcodes = drizzle_parse_shortcode_atts($content, '[drizzle]', '[/drizzle]');
  if (!array_key_exists('content', $shortcodes)) {
    return;
  }
  $targetstring = '[drizzle]' . $shortcodes['content'][0] . '[/drizzle]';
  $content = trim(trim($shortcodes['content'][0]), '\n');
  if (!$content) {
    return;
  }
  else {
    $node->content['body'][0]['#markup'] = str_replace($targetstring, '<div id="zenmarket--wrapper" style="display: none;"></div>', $node->body[$langcode][0]['value']);
  }
}

/**
 * Helper function to drizzle_build_query.
 */
function drizzle_build_query(array $args) {
  return drizzle_http_build_query($args, '', '&');
}

/**
 * Helper function to build http_query, coppied from WP.
 */
function drizzle_http_build_query($data, $prefix = NULL, $sep = NULL, $key = '', $urlencode = TRUE) {
  $ret = array();
  foreach ((array) $data as $k => $v) {
    if ($urlencode) {
      $k = urlencode($k);
    }
    if (is_int($k) && $prefix != NULL) {
      $k = $prefix . $k;
    }
    if (!empty($key)) {
      $k = $key . '%5B' . $k . '%5D';
    }
    if ($v === NULL) {
      continue;
    }
    elseif ($v === FALSE) {
      $v = '0';
    }
    if (is_array($v) || is_object($v)) {
      array_push($ret, drizzle_http_build_query($v, '', $sep, $k, $urlencode));
    }
    elseif ($urlencode) {
      array_push($ret, $k . '=' . urlencode($v));
    }
    else {
      array_push($ret, $k . '=' . $v);
    }
  }
  return implode($sep, $ret);
}

/**
 * Helper function to check status and validate the key from drizzle.
 */
function drizzle_check_key_status($key) {
  global $base_url;
  return drizzle_http_post(drizzle_build_query(array('key' => $key, 'blog' => $base_url)), 'verify-key');
}

/**
 * Get subscription plans.
 */
function drizzle_get_drizzle_plans($key) {
  global $base_url;
  return drizzle_http_post(drizzle_build_query(array('key' => $key, 'url' => $base_url)), 'get-plans');
}

/**
 * Set plans to individual contents.
 */
function drizzle_set_drizzle_plans($key, $url, $plan_id, $nid) {
  $args = array(
    'key' => $key,
    'url' => $url,
    'planId' => $plan_id,
    'id' => $nid,
  );
  return drizzle_http_post(drizzle_build_query($args), 'set-plan');
}

/**
 * Get Wall plans to individual contents.
 */
function drizzle_get_drizzle_wall_plans() {
  global $base_url;
  $key = drizzle_get_api_key();
  return drizzle_http_post(drizzle_build_query(array('key' => $key, 'url' => $base_url)), 'get-walls');
}

/**
 * Helper function to verify key from drizzle.
 */
function drizzle_verify_key($key) {
  $response = drizzle_check_key_status($key);
  if ($response[1] != 'valid' && $response[1] != 'invalid') {
    return 'failed';
  }
  return $response[1];
}

/**
 * Make a POST request to the Drizzle API.
 */
function drizzle_http_post($request, $path) {
  global $is_https;
  $drizzle_ua = sprintf('Drupal/%s | Paywall/%s', '7.x', DRIZZLE_VERSION);
  $host = DRIZZLE_API_HOST;
  $http_host = DRIZZLE_API_URL;
  $http_args = array(
    'data' => $request,
    'method' => 'POST',
    'headers' => array(
      'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8',
      'Host' => $host,
      'User-Agent' => $drizzle_ua,
    ),
    'httpversion' => '1.0',
    'timeout' => 15,
  );
  $drizzle_url = "{$http_host}/{$path}";
  // Check if SSL requests were disabled fewer than X hours ago.
  $ssl_disabled = TRUE;
  if ($is_https) {
    $ssl_disabled = FALSE;
  }
  if (!$is_https) {
    $ssl_disabled = FALSE;
  }
  $ssl_failed = FALSE;
  if (!$ssl_disabled) {
    $response = drupal_http_request($drizzle_url, $http_args);
  }
  if ($ssl_disabled || $ssl_failed) {
    $http_args['sslverify'] = FALSE;
    $response = drupal_http_request($drizzle_url, $http_args);
  }
  if (isset($response->data)) {
    if ($response->data == 'invalid') {
      return array('', '');
    }
    $simplified_response = array($response->headers, $response->data);
    return $simplified_response;
  }
}

/**
 * Get API if set or empty if none.
 */
function drizzle_get_api_key() {
  return variable_get('drizzle_key', '');
}

/**
 * Parse shortcode.
 */
function drizzle_parse_shortcode_atts($string, $start, $end) {
  $result = array();
  $shortcode_atts = array();
  $string = " " . $string;
  $offset = 0;
  while (TRUE) {
    $ini = strpos($string, $start, $offset);
    if ($ini == 0) {
      break;
    }
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    $result[] = substr($string, $ini, $len);
    $offset = $ini + $len;
  }
  if ($result) {
    $shortcode_atts["code"] = 'drizzle';
    $shortcode_atts["content"] = $result;
  }
  return $shortcode_atts;
}

/**
 * Implements hook_form_alter().
 */
function drizzle_form_alter(&$form, &$form_state, $form_id) {
  $key = drizzle_get_api_key();
  if ((arg(0) == 'node' && (arg(1) == 'add' || arg(2) == 'edit')) && $key) {
    $lang = 'und';
    if (isset($form['body']['#language'])) {
      $lang = $form['body']['#language'];
    }
    if (isset($form['body'][$lang][0]['#title'])) {
      $btitle = $form['body'][$lang][0]['#title'];
      $form['body'][$lang][0]['#title'] = $btitle . ' (<span class="description" style="color:#666">DRIZZLE ENABLED: You can use Drizzle code here like [drizzle] your content [/drizzle] </span>)';
    }
  }
  if (arg(0) == 'node' && arg(2) == 'edit' && $key) {
    $form['actions']['submit']['#submit'][] = '_drizzle_node_submit';
  }
}

/**
 * Node submit function.
 */
function _drizzle_node_submit($form, &$form_state) {
  $node = node_form_submit_build_node($form, $form_state);
  $key = drizzle_get_api_key();
  $language = key($node->body);
  $content = '';
  if ($node->nid) {
    if (isset($node->body[$language][0]['value'])) {
      $content = $node->body[$language][0]['value'];
    }
    if (empty($content)) {
      return;
    }
    $shortcodes = drizzle_parse_shortcode_atts($content, '[drizzle]', '[/drizzle]');
    if (!array_key_exists('content', $shortcodes)) {
      return;
    }
    $content = trim(trim($shortcodes['content'][0]), '\n');
    $content = _filter_autop($content);
    if (!$content) {
      return;
    }
    $options = array('absolute' => TRUE);
    $nid = $node->nid;
    $node_url = url('node/' . $nid, $options);
    $query = drizzle_build_query(array(
      'key' => $key,
      'url' => $node_url,
      'title' => $node->title,
      'content' => $content,
      'id' => $nid,
    ));
    $response = drizzle_http_post($query, 'create-wall');
    if (empty($response[1])) {
      return;
    }
    $response = json_decode($response[1]);
    if (!$response) {
      return;
    }
  }
}

/**
 * Helper function to remove protocol.
 */
function drizzle_remove_http($url) {
  $disallowed = array('http://', 'https://');
  foreach ($disallowed as $d) {
    if (strpos($url, $d) === 0) {
      return str_replace($d, '', $url);
    }
  }
  return $url;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function drizzle_form_node_admin_content_alter(&$form, &$form_state, $form_id) {
  if (arg(2) == 'drizzle') {
    $key = drizzle_get_api_key();
    $valid_key = drizzle_verify_key($key);
    if ($key && $valid_key == 'valid') {
      $column_alias = t('drizzle_status');
      $column_title = t('Drizzle Status');
      $column_alias_sub = t('drizzle_subscription');
      $column_title_sub = t('Subscription plan');
      drupal_set_message(t("Please make sure that your content has multiple &lt;p&gt; tags, otherwise Drizzle's automatic feature will not work properly."), 'warning', FALSE);
      $form['filter']['#weight'] = -2;
      unset($form['admin']['options']['submit']['#submit'][0]);
      unset($form['admin']['options']['operation']['#default_value']);
      $form['admin']['options']['operation']['#access'] = FALSE;
      $form['admin']['options']['operation']['#default_value'] = FALSE;
      $form['admin']['options']['submit']['#submit'][] = 'drizzle_mark_node_50';
      $form['admin']['options']['drizzle_hide_para'] = array(
        '#type' => 'select',
        '#title' => t('Hide content after numbe of paragraphs'),
        '#options' => array(
          0 => t('Unhide'),
          1 => t('Hide from 1st paragraph'),
          2 => t('Hide from 2nd paragraph'),
          3 => t('Hide from 3rd paragraph'),
          4 => t('Hide from 4th paragraph'),
          5 => t('Hide from 5th paragraph'),
          6 => t('Hide from 6th paragraph'),
          7 => t('Hide from 7th paragraph'),
          8 => t('Hide from 8th paragraph'),
          9 => t('Hide from 9th paragraph'),
          10 => t('Hide from 10th paragraph'),
        ),
        '#default_value' => 0,
        '#weight' => -1,
        '#title_display' => 'invisible',
      );
      $plansname['_none'] = t('Remove Plan');
      $plans = drizzle_get_drizzle_plans($key);
      $plans = json_decode($plans[1]);
      if ($plans->status != 'invalid') {
        if (is_object($plans)) {
          $plans = $plans->plans;
        }
        foreach ($plans as $plan) {
          $plansname[$plan->_id] = $plan->name;
        }
      }
      $form['drizzle_subscriptions'] = array(
        '#type' => 'fieldset',
        '#title' => t('Subscriptions Plans'),
        '#attributes' => array('class' => array('container-inline')),
        '#access' => 1,
        '#weight' => 0,
      );
      $form['drizzle_subscriptions']['plans'] = array(
        '#type' => 'select',
        '#title' => t('Subscribe To Plans'),
        '#options' => $plansname,
        '#default_value' => 0,
        '#title_display' => 'invisible',
      );
      $form['drizzle_subscriptions']['assign_plan'] = array(
        '#type' => 'submit',
        '#value' => t('Assign Plan'),
        '#validate' => array('node_admin_nodes_validate'),
      );
      $form['drizzle_subscriptions']['assign_plan']['#submit'][] = 'drizzle_assign_plan_submit';
      if (!empty($column_alias_sub)) {
        $form['admin']['nodes']['#header'][$column_alias_sub] = array('data' => $column_title_sub);
      }
      $form['admin']['nodes']['#header'][$column_alias] = array('data' => $column_title);
      $json = drupal_json_decode(variable_get('drizzle_50', ''));
      $getwall_plans = drizzle_get_drizzle_wall_plans();
      $wall_plans = array();
      $wall_plans = json_decode($getwall_plans[1]);
      if ($wall_plans->status == 'valid') {
        $wall_plans = $wall_plans->walls;
      }
      foreach ($form['admin']['nodes']['#options'] as $nid => $row) {
        $drizzle_50 = 'Disabled';
        $drizzle_sub_status = '---';
        if (is_array($json) && in_array($nid, $json)) {
          $drizzle_50 = '<span style="color:green">Enabled</span>';
        }
        $node_url = url('node/' . $nid, array('absolute' => TRUE));
        $url = drizzle_remove_http($node_url);
        if (!empty($wall_plans)) {
          foreach ($wall_plans as $plan) {
            if (is_object($plan)) {
              if ($plan->externalId == $url . '/' || $plan->externalId == $nid) {
                $drizzle_sub_status = '<strong style="color:green">' . $plan->planName . '</strong>';
              }
            }
          }
        }
        $form['admin']['nodes']['#options'][$nid][$column_alias] = $drizzle_50;
        $form['admin']['nodes']['#options'][$nid][$column_alias_sub] = $drizzle_sub_status;
      }
    }
  }
}

/**
 * Submit function for assign plans to content.
 */
function drizzle_assign_plan_submit($form, &$form_state) {
  $key = drizzle_get_api_key();
  $nodes = $form_state['values']['nodes'];
  $plan_id = $form_state['values']['plans'];
  if (!empty($plan_id)) {
    if ($plan_id == '_none') {
      $plan_id = '';
    }
    foreach ($nodes as $node) {
      $node = node_load($node);
      if (is_object($node)) {
        $node_url = url('node/' . $node->nid, array('absolute' => TRUE));
        $result = drizzle_set_drizzle_plans($key, $node_url, $plan_id, $node->nid);
        $response = json_decode($result[1]);
        if ($response->status != 'valid') {
          drupal_set_message(t("There is some issue in setting plan for @title.", array("@title" => $node->title)), 'error');
        }
        else {
          drupal_set_message(t("@title successfully set to plan.", array("@title" => $node->title)), 'status');
        }
      }
    }
  }
  $form_state = form_state_defaults();
}

/**
 * Submit function for assign plans to content.
 */
function drizzle_mark_node_50($form, &$form_state) {
  $para = $form_state['values']['drizzle_hide_para'];
  $nodes = $form_state['values']['nodes'];
  if ($para == 0) {
    $para = $form_state['values']['drizzle_hide_para'];
    $nodes = $form_state['values']['nodes'];
    drizzle_node_unhide_para($nodes, $para);
  }
  else {
    drizzle_node_hide_para($nodes, $para);
  }
  $form_state = form_state_defaults();
}

/**
 * Hide no of para.
 */
function drizzle_node_hide_para($nodes, $para) {
  $marked_nodes = drupal_json_decode(variable_get('drizzle_50', ''));
  foreach ($nodes as $node) {
    $marked_nodes[$node] = $node;
    drizzle_50_percent($node, 'mark', $para);
  }
  $json = drupal_json_encode($marked_nodes);
  variable_set('drizzle_50', $json);
}

/**
 * Unhide para.
 */
function drizzle_node_unhide_para($nodes, $para) {
  $marked_nodes = drupal_json_decode(variable_get('drizzle_50', ''));
  foreach ($nodes as $node) {
    if ($node) {
      unset($marked_nodes[$node]);
      drizzle_50_percent($node, 'unmark', '');
    }
  }
  $json = drupal_json_encode($marked_nodes);
  variable_set('drizzle_50', $json);
}

/**
 * Helper function to avail 50% of content to drizzle.
 */
function drizzle_50_percent($nid, $op, $para) {
  $language = '';
  $key = drizzle_get_api_key();
  $node = node_load($nid);
  if (is_object($node)) {
    if (is_array($node->body) && !empty($node->body)) {
      $language = key($node->body);
    }
  }
  $content = '';
  if (isset($node->body[$language][0]['value'])) {
    $string = $node->body[$language][0]['value'];
    // Replacing [drizzle] tags if any and making pure string.
    $s1 = str_replace('[drizzle]', "", $string);
    $s2 = str_replace('[/drizzle]', "", $s1);
    $s3 = str_replace('<p>[drizzle]</p>', "", $s2);
    $purestring = str_replace('<p>[/drizzle]</p>', "", $s3);
    $purestring = _filter_autop($purestring);
    $drizzlecontent = '';
    if ($op == 'mark') {
      if (drizzle_ishtml($string)) {
        $content_array = drizzle_trimpara($purestring, $para);
        $content = $content_array['first_part'] . '[drizzle]' . $content_array['second_part'] . '[/drizzle]';
        $drizzlecontent = $content_array['second_part'];
      }
      else {
        $stringhalf = round(strlen($purestring) / 2);
        $firsthalf = drizzle_plainstring($purestring, $stringhalf);
        $secondhalf = str_replace($firsthalf, "", $purestring);
        $content = $firsthalf . '[drizzle]' . $secondhalf . '[/drizzle]';
        $drizzlecontent = $secondhalf;
      }
      $node->body[$language][0]['value'] = $content;
      field_attach_update('node', $node);
      // Clear the static loading cache.
      entity_get_controller('node')->resetCache(array($node->nid));
      $options = array('absolute' => TRUE);
      $nid = $node->nid;
      $node_url = url('node/' . $nid, $options);
      $query = drizzle_build_query(array(
        'key' => $key,
        'url' => $node_url,
        'title' => $node->title,
        'content' => $drizzlecontent,
        'id' => $nid,
      ));
      $response = drizzle_http_post($query, 'create-wall');
      if (empty($response[1])) {
        drupal_set_message(t("There is some issue in enabling drizzle for @title", array("@title" => $node->title)), 'error');
      }
      $response = json_decode($response[1]);
      if ($response->status == 'valid') {
        drupal_set_message(t("@title successfully marked as hidden from paragraph no. <strong> @para </strong>.", array("@title" => $node->title, "@para" => $para)), 'status');
      }
    }
    elseif ($op == 'unmark') {
      $node->body[$language][0]['value'] = $purestring;
      field_attach_update('node', $node);
      // Clear the static loading cache.
      entity_get_controller('node')->resetCache(array($node->nid));
      drupal_set_message(t("@title successfully marked unhide.", array("@title" => $node->title)), 'status');
    }
  }
}

/**
 * Helper function to check string is html.
 */
function drizzle_ishtml($text) {
  $processed = htmlentities($text);
  if ($processed == $text) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Helper function to trim no of paragraph.
 */
function drizzle_trimpara($text, $number_para) {
  $number_para = $number_para + 1;
  $html = htmlspecialchars($text);
  $parts = preg_split('#&lt;p([^&lt;])#', $html);
  $cntto3p = "";
  $cont_at3p = "";
  if (!empty($parts[0]) && !empty($parts[1])) {
    $initial_content = $parts[0];
    $cntto3p .= html_entity_decode($initial_content);
    for ($i = 1; $i < count($parts); $i++) {
      if ($i < $number_para) {
        $cntto3p .= '<p ' . html_entity_decode($parts[$i]);
      }
      else {
        $cont_at3p .= '<p ' . html_entity_decode($parts[$i]);
      }
    }
  }
  else {
    $parts = preg_split('#<p()#', $text);
    for ($i = 1; $i < count($parts); $i++) {
      if ($i < $number_para) {
        $cntto3p .= '<p ' . $parts[$i];
      }
      else {
        $cont_at3p .= '<p ' . $parts[$i];
      }
    }
  }
  $content = array('first_part' => $cntto3p, 'second_part' => $cont_at3p);
  return $content;
}

/**
 * Helper function to trim plain string.
 */
function drizzle_plainstring($code, $limit = 300) {
  if (strlen($code) <= $limit) {
    return $code;
  }
  $html = substr($code, 0, $limit);
  preg_match_all("#<([a-zA-Z]+)#", $html, $result);
  foreach ($result[1] as $key => $value) {
    if (strtolower($value) == 'br') {
      unset($result[1][$key]);
    }
  }
  $openedtags = $result[1];
  preg_match_all("#</([a-zA-Z]+)>#iU", $html, $result);
  $closedtags = $result[1];
  foreach ($closedtags as $key => $value) {
    if (($k = array_search($value, $openedtags)) === FALSE) {
      continue;
    }
    else {
      unset($openedtags[$k]);
    }
  }
  if (empty($openedtags)) {
    if (strpos($code, ' ', $limit) == $limit) {
      return $html;
    }
    else {
      return substr($code, 0, strpos($code, ' ', $limit));
    }
  }
  $position = 0;
  $close_tag = '';
  foreach ($openedtags as $key => $value) {
    $p = strpos($code, ('</' . $value . '>'), $limit);
    if ($p === FALSE) {
      $code .= ('</' . $value . '>');
    }
    elseif ($p > $position) {
      $close_tag = '</' . $value . '>';
      $position = $p;
    }
  }
  if ($position == 0) {
    return $code;
  }
  return substr($code, 0, $position) . $close_tag;
}
