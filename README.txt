Welcome to Simple paywall
Contributors: getdrizzle
Donate link: https://getdrizzle.com/
version : 7.x-3.4
Tags: paywall, membership, micropayments, payment, recurring payments,
paid subscriptions, direct monetization, premium content, paid content,
monetization, content monetization, sell content, pay per view, pay as you go,
lead gen, lead generation, drizzle, video.
Simple module for bloggers and publishers to monetize premium content with two
user-friendly, one-click options: micropayments and paid subscriptions.

-- Description --

This module is a full-stack solution for pro bloggers and publishers to start
monetizing their best content directly from website visitors. Once installed,
you have your own membership, paywall, lead generation, payment,
purchase analytics and dynamic revenue-maximizing systems.

This module will let you paywall virtually any content in your posts.
You can paywall text, images, videos, links, and any HTML-elements.
Your website visitors will be offered to pay for your content with just
one-click, either in a "pay-as-you-go" fashion via micropayments or
via paid subscriptions.

The module works with a simple shortcode:

[drizzle] Your great content. [/drizzle]


If you are a pro blogger or publisher who creates great content which
people love, then keep reading. This module allows you to:

1. Paywall virtually any content: text, podcast, images, video, etc.
Website visitors are one-click away from signing up and one-click away from
paying for your content. Visitors are offered 2 user-friendly,
one-click payment options: either a single micropayment or a recurring
subscription payment.

1. Pick the best performing content. Not sure which content to paywall?
Not a problem. The performance of each content you paywall is monitored,
and if the content underperforms, the paywall will be removed automatically.
Just specify the number of $ you want to earn per 1000 impressions.

1. Pick the price. Not sure how to price your content? Not a problem.
Every paywalled content gets a default price. If your content performs well,
the price is increased automatically to maximize your revenue.

The content's popularity strongly depends on 2 metrics: the number of people who
purchased, the number of people who recommended and the number of people who
shared the content.

1. Set up paid subscriptions with just one click.

1. Keep all your users' information. When a registered Drizzle user interacts
with your content, you keep the user's name, email and other applicable
attributes, such as purchase history. Seamless lead generation!

1. Promote your content by allowing users who purchased your content to
share it for free with their friends via email. 

1. Upsell your best content on your website and other websites in
Drizzle's network. Registered users on your website can easily see a list of the
top recommended and top similar content on your website.
If your content does really well, your content may go viral and be featured on
other websites in the Drizzle network.

The module is built by Drizzle. Learn more: https://getdrizzle.com

-- INSTALLATION --

This section describes how to install the module and how to use it.

1. Place the entirety of this directory in sites/all/modules/.
2. Navigate to administer >>  modules. Enable Simple paywall, membership,
micropayments and paid subscriptions (by Drizzle).
3. Navigate to administer >> config >> drizzle and
Register at https://getdrizzle.com
4. Generate API key at "Setup" tab. Copy key.
5. Paste API key on text field and save configuration. !

1, 2, 3, 4, 5: You're done!

-- CONFIGURATION --

* Configure API key in administer >>  config >> drizzle:

-- TROUBLESHOOTING --

* If the menu does not display, check the following:

- Are the "Access administration menu" and "Use the administration
pages and help"
permissions enabled for the appropriate roles?

- Does html.tpl.php of your theme output the $page_bottom variable?

* If the menu is rendered behind a Flash movie object, add this property to your
  Flash object(s):

  <param name="wmode" value="transparent" />

  See http://drupal.org/node/195386 for further information.
  
-- FAQ --

Q: Why should I monetize my content directly?

A: Direct monetization can serve as a complement to a website’s existing streams
of revenue. We’ve learned that website visitors love to pay for high quality
content, but they want to do so in simple, user-friendly way. There’s no need to
alter any existing revenue sources, such as sponsored content or banner ads.
With Drizzle, you’ll also learn which content your users value most,
gain an effective new source of lead generation, and upsell your content to
more engaged audiences. Your top performing content can generate up to
$20 per 1000 impressions.Compare that to ads, with only $2 CPM.

Q: Do I get my users' information?

A: Yes, you do. Any time a website visitor interacts with your content,
either via payment, subscription, recommendation or “get for free” actions,
you will get that user’s name, email address and other applicable attributes.

Q: How much of my content will be considered premium?

A: You have to make an educated guess on whether your content will sell well
once it’s paywalled. We strongly suggest you keep most of your content free and
paywall unique content only, the kind of content people cannot find anywhere
else but your website. Once website visitors start interacting your paywalled
content, the module will closely monitor the performance of that content.
If the content underperforms the paywall is automatically removed.
The module calculates performance based on a combination of the number of people
who recommended the content and the number people who purchased the content.

Q: Can I use this module for lead generation?

A: Yes, we’re so glad you asked! The module doubles as a lead generation tool by
giving you the name, email address, and purchase history of users who purchase
your paywalled content.

Q: What type of payments does this module offer?

A: The module allows publishers to choose between a-la-carte or paid
subscriptions. Bloggers and publishers can offer both options to their website
visitors. A-la-carte paywalls mean that users micropay for each individual piece
of content they’d like to access, where as subscriptions mean that users pay a
monthly fee to access all of a website’s premium content.

Q: I already receive revenue from banner ads.
Will I be forced to forfeit that if I sign up with this module?

A: Nope. The beauty of Drizzle is that it serves as a complement to your
existing revenue streams. We recognize that most content makers lose banner
ad revenue due to the popularity of Adblock.

Q: Got more questions?

A: Learn more: http://drizzle.helpscoutdocs.com/
